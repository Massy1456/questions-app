There are a few steps you need to follow in order to get the Webhooks working.

1. You need to create an Instagram Business Account. This will enable you trigger the event of an incoming message and then respond.
2. You need to create an 'App' on the *Meta For Developers* website.
3. You then need to add the Webhooks of your choosing. For this instance, Instagram and Messenger.
4. You will need to verify your server by using a secured endpoint. I used `ngrok` for simplicity.
	1. Set a verification token that you can remember and then set the Callback URL to your prefered endpoint
	2. The code for this is included

![[Pasted image 20220621112010.png]]

5. Make sure to subscribe to all the various Webhooks you wish to use on the platform `messages`, `messaging_handover`, `mentions`, etc
6. Test it out with your server
7. The messages will come out like so from `req.body.entry[0].messaging`:
```
[
  {
    sender: { id: '5538844716139631' },
    recipient: { id: '116134537736625' },
    timestamp: 1655827486554,
    message: {
      mid: 'm_jfYCPxV7XJhtMhdcKd6TOqj_lN9kjM5uCyoNCZOOgxsGdwuooZaVBf0k2bJXZof5tKGAa40qykFP5oFvgfrfeQ',
      text: 'Hello'
    }
  }
]
```



----

# Working Output
### Instagram Messenger

![alt text](/images/chrome_T9JKkJ18JR.png)

---
### Messenger

![alt text](/images/chrome_T9JKkJ18JR.png)
![alt text](/images/chrome_T9JKkJ18JR.png)
