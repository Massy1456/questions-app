const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const dotenv = require('dotenv')
dotenv.config()

const questionRoutes = require('./routes/Question')

const app = express()

app.use(express.urlencoded({ extended: false }))
app.use(bodyParser.json({limit: "30mb", extended: false}))
app.use(bodyParser.urlencoded({limit: "30mb", extended: false}))
app.use(cors())

app.use(questionRoutes)

const CONNECTION_URL = process.env.CONNECTION_URL
const PORT = process.env.PORT

// create connection with database
mongoose.connect(CONNECTION_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,}
    ).then(() => app.listen(PORT, () => console.log(`Server up on Port ${PORT}`))
    ).catch((error) => console.log(error.message))
