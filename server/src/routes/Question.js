const express = require('express')
const { postQuestion, getQuestions } = require('../controllers/Question')

const router = express.Router()

router.post('/question', postQuestion)

router.get('/question', getQuestions)

module.exports = router

