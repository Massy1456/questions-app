const Question = require('../models/Question')

const postQuestion = async (req, res) => {
    try {
        const { username, question } = req.body
        const newQuestion = new Question({username, question})
        await newQuestion.save()
        res.status(201).send(newQuestion)
    } catch (e) {
        res.status(500).send(e)
    }
}

const getQuestions = async (req, res) => {
    try {
        const questions = await Question.find({})
        if (!questions) return res.status(404).send({message:'no questions found'})
        res.status(200).send(questions)
    } catch (e) {
        res.status(500).send(e)
    }
}

module.exports = {
    postQuestion,
    getQuestions
}